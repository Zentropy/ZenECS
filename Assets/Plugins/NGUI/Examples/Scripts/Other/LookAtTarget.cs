﻿using UnityEngine;

/// <summary>
/// Attaching this script to an object will make that object face the specified target.
/// The most ideal use for this script is to attach it to the camera and make the camera look at its target.
/// </summary>

[AddComponentMenu("NGUI/Examples/Look At Target")]
public class LookAtTarget : MonoBehaviour
{
	public int level = 0;
	public Transform target;
	public float speed = 8f;
    float speedActual = 0f;
    public float height = 0f;

	Transform mTrans;
    public float startDelay = 0f;

	void Start ()
	{
		mTrans = transform;
	}

    public void setTarget(Transform t)
    {
        speedActual = 0f;
        target = t;
    }

	void LateUpdate ()
	{
		if (target != null && Time.timeSinceLevelLoad > startDelay)
		{
			Vector3 dir = target.position - mTrans.position + Vector3.up * height;
			float mag = dir.magnitude;
            speedActual = Mathf.Lerp(speedActual, speed, Time.deltaTime);

            if (mag > 0.001f)
			{
				Quaternion lookRot = Quaternion.LookRotation(dir);
				mTrans.rotation = Quaternion.Slerp(mTrans.rotation, lookRot, Mathf.Clamp01(speedActual * Time.deltaTime));
			}
		}
	}
}