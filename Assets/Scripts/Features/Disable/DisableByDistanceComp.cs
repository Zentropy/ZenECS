﻿using Sirenix.OdinInspector;

namespace Zen.Components
{
	using System.Collections.Generic;
	//using AdvancedInspector;
	using Common.ZenECS;
	using UnityEngine;

	public class DisableByDistanceComp : ComponentEcs
	{
		[ShowInInspector]
		public bool IsActive { get; set; }

		[ShowInInspector] 
		public bool DisableRenderers { get; set; }
		[ShowInInspector]
		public bool DisableScripts { get; set; }
		[ShowInInspector]
		public bool DisableParticles { get; set; }
		[ShowInInspector]
		public bool DisableCollisions { get; set; }

		public List<Renderer> allRenderers = new List<Renderer>();
		public List<MonoBehaviour> allBehaviours = new List<MonoBehaviour>();
		public List<ParticleSystem> allParticles = new List<ParticleSystem>();
		public List<Collider> allColliders = new List<Collider>();

		public override ComponentTypes ComponentType => ComponentTypes.DisableByDistanceComp;
	}
}