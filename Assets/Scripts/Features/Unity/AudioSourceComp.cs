﻿// /** 
//  * AudioSourceComp.cs
//  * Dylan Bailey
//  * 20161209
// */

namespace Zen.Components
{
    #region Dependencies

    using UnityEngine;

    using Zen.Common.ZenECS;

    #endregion

    public class AudioSourceComp : ComponentEcs
    {
        public AudioSource AudioSource;
        public override ComponentTypes ComponentType => ComponentTypes.AudioSourceComp;
	    public override string Grouping => "Unity";

    }
}