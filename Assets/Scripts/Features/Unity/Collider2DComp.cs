﻿// /** 
//  * ColliderComp.cs
//  * Dylan Bailey
//  * 20161209
// */

namespace Zen.Components
{
	#region Dependencies

	using UnityEngine;
	using Zen.Common.ZenECS;

	#endregion

	public class Collider2DComp : ComponentEcs
	{
		public Collider2D collider;

		public override ComponentTypes ComponentType => ComponentTypes.Collider2DComp;
		public override string Grouping => "Unity";
	}
}