﻿// /** 
//  * CollisionEnterComp.cs
//  * Dylan Bailey
//  * 20161210
// */

namespace Zen.Components
{
	#region Dependencies

	using System.Collections.Generic;
	using UnityEngine;
	using Zen.Common.ZenECS;

	#endregion

	public class Collision2DEnterComp : AbstractCollisionComp
	{
		public List<Collision2D> Other = new List<Collision2D>(5);

		public override void OnDestroy()
		{
			Other.Clear();
		}

		public override ComponentTypes ComponentType => ComponentTypes.Collision2DEnterComp;
		public override string Grouping => "Unity";
	}
}