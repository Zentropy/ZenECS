﻿// /** 
//  * UnityPrefabComp.cs
//  * Dylan Bailey
//  * 20161209
// */

using Sirenix.OdinInspector;

namespace Zen.Components
{
    #region Dependencies

	//using AdvancedInspector;
    using UnityEngine;
	using Zen.Common.Extensions;
	using Zen.Common.ZenECS;

    #endregion

    public class UnityPrefabComp : ComponentEcs
    {
        [HideInEditorMode, HideInPlayMode] 
	    //[TextField(TextFieldType.Prefab)]
        public string PrefabLink = "";
	    
	    
	    private string[] PrefabList => UnityDrawerStatics.PrefabList;

	    [ValueDropdown("PrefabList"), OnValueChanged("PrefabChosenCallback")] public string PrefabChosen;

	    public void PrefabChosenCallback() {
		    Debug.Log("Prefab is: " + PrefabChosen);
		    PrefabLink = PrefabChosen;
	    }

        [ShowInInspector]
        public bool IsPooled { get; set; }

		//[ShowInInspector]public UnityLayer layer { get; set; } = new UnityLayer(0);
	    public LayerMask layer;

	    public Tags tags;
	    public EntityTags entityTags;

	    public override string ToString()
	    {
		    return "UnityPrefabComp";
	    }

        public override ComponentTypes ComponentType => ComponentTypes.UnityPrefabComp;
	    public override string Grouping => "Unity";
    }
}