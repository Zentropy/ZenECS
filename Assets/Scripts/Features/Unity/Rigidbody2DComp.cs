﻿// /** 
//  * Rigidbody2DComp.cs
//  * Dylan Bailey
//  * 20161209
// */

namespace Zen.Components
{
	#region Dependencies

	using UnityEngine;
	using UnityEngine.Assertions;
	using Zen.Common.ZenECS;

	#endregion

	public class Rigidbody2DComp : ComponentEcs
	{
		public Rigidbody2D rigidbody;

		public bool UseDefaults = true;

		public float mass = 1f;
		public float drag = 0f;
		public float angularDrag = 0.05f;
		public float gravityScale = 1.0f;
		public bool isKinematic = false;

		public RigidbodyInterpolation2D interpolation;
		public CollisionDetectionMode2D collisionDetectionMode;
		public RigidbodyConstraints2D constraints;
		public float maxAngularVelocity = 7f;

		public override void InitialiseLate(EcsEngine _engine, Entity owner)
		{
			Assert.IsNotNull(rigidbody, "Rigidbody2D null in comp initialize");
			if (UseDefaults) return;

			rigidbody.mass = mass;
			rigidbody.drag = drag;
			rigidbody.angularDrag = angularDrag;
			rigidbody.gravityScale = gravityScale;
			rigidbody.isKinematic = isKinematic;
			rigidbody.interpolation = interpolation;
			rigidbody.collisionDetectionMode = collisionDetectionMode;
			rigidbody.constraints = constraints;
			
		}

		public override ComponentTypes ComponentType => ComponentTypes.Rigidbody2DComp;
		public override string Grouping => "Unity";
	}
}