﻿// /** 
//  * CollisionCleanupSystem.cs
//  * Dylan Bailey
//  * 20161210
// */

using UnityEngine;

namespace Zen.Systems
{
    #region Dependencies

    using Zen.Common.ZenECS;
    using Zen.Components;

    #endregion

    public class CollisionDebugSystem : AbstractEcsSystem
    {
        public override bool Init()
        {
            return true;
        }

        public override void Update()
        {
            var collenter = engine.Get(ComponentTypes.CollisionEnterComp);
            foreach (CollisionEnterComp coll in collenter)
            {
                if (coll.Other.Count > 0)
                    Debug.Log($"System found {coll.Owner} colliding ");
                    //_totalCollisions++;
            }

            var collexit = engine.Get(ComponentTypes.CollisionExitComp);
            foreach (CollisionExitComp coll in collexit)
            {
                if (coll.Other.Count > 0)
                    Debug.Log($"System found {coll.Owner} UNcolliding ");
                    //_totalCollisions++;
            }
        }
    }
}