﻿using Zen.Common.ZenECS;

namespace Zen.Systems
{
	using Common;
	using UnityEngine;
	using Zen.Components;

    public class GameInitSystem : AbstractEcsSystem
	{
	   
		public override bool Init()
		{
			engine.AddSystem(new EntityTagInitSystem())
			      .AddSystem(new PooledEntityInitSystem());

			var cam = GameObject.FindGameObjectWithTag("MainCamera");
			Object.Destroy(cam);
			
			engine.CreateEntity(Res.Entities.Player);
			engine.CreateEntity(Res.Entities.Camera);
			engine.CreateEntity(Res.Entities.SectorGenerationMain);
			engine.CreateEntity(Res.Entities.GameSettings);
			//var ets = engine.CreateEntity(Res.Entities.Enemy);
			//ets.Wrapper.transform.position = new Vector3(0, 0, 10);
            //ets = engine.CreateEntity(Res.Entities.Enemy);
            //ets.Wrapper.transform.position = new Vector3(10, 0, 10);
            //ets = engine.CreateEntity(Res.Entities.Enemy);
            //ets.Wrapper.transform.position = new Vector3(0, 10, 10);
		    InitSystems();
		    EntityPool.Instance.ResetEntityPool();
		    return false;
            
		}

	    private void InitSystems()
	    {
	        engine.AddSystem(new PlayerInitSystem())
	            .AddSystem(new SectorGenerationSystem())
	            .AddSystem(new ShipInitSystem())
	            .AddSystem(new PlayerInputSystem())
	            .AddSystem(new PlayerMovementSystem())
	            .AddSystem(new PlayerTargetingSystem())
	            .AddSystem(new PositionUpdateSystem())
	            .AddSystem(new CameraControlSystem())
	            .AddSystem(new RangedCombatSystem())
	            .AddSystem(new MissileFlightSystem())
	            .AddSystem(new MissileCollisionResolverSystem())
	            .AddSystem(new MissileAreaDamageSystem())
	            .AddSystem(new MissileExplosionSystem())
	            .AddSystem(new ShipDamageSystem())
	            .AddSystem(new TrackingCrosshairSystem())
	            .AddSystem(new DeathRemovalSystem())
	            .AddSystem(new InertialDamperSystem())
                .AddSystem(new CollisionDebugSystem())
	            .AddSystem(new CollisionCleanupSystem())

	            //Reactives
	            ;

	        engine.InitAfterECS();
	    }
	}
}