﻿// /** 
//  * PositionComp.cs
//  * Dylan Bailey
//  * 20161209
// */

using Sirenix.OdinInspector;

namespace Zen.Components
{
    #region Dependencies

    using System;
    //using AdvancedInspector;
    using UnityEngine;
    using Zen.Common.ZenECS;

    #endregion

    public class PositionComp : ComponentEcs
    {
        //PH, we'll want to inject this from the Entity
        [NonSerialized] public Transform transform;
        public Vector3 Position { get; set; }
        public Quaternion Rotation { get; set; }
		[ShowInInspector]public bool UseLateUpdate { get; set; }

        public override ComponentTypes ComponentType => ComponentTypes.PositionComp;
    }
}