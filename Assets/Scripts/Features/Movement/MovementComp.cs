﻿// /** 
//  * MovementComp.cs
//  * Will Hart
//  * 20161103
// */

using Sirenix.OdinInspector;

namespace Zen.Components
{
    #region Dependencies

	//using AdvancedInspector;
	using Common.ZenECS;

    #endregion

    public class MovementComp : ComponentEcs
    {
        public float MoveSpeed {get; set;} = 5;
        public float CurrentMoveSpeed {get; set;}
        public MovementType MovementType  { get; set; } = MovementType.Ground;
		[ShowInInspector]public bool UseFixedUpdateMovement { get; set; }

        public override ComponentTypes ComponentType => ComponentTypes.MovementComp;
    }

    public enum MovementType
    {
        Ground,
        Air
    }
}