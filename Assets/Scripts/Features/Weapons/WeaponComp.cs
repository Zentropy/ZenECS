﻿// /** 
//  * WeaponComp.cs
//  * Dylan Bailey
//  * 20161103
// */

using Sirenix.OdinInspector;

namespace Zen.Components
{
	#region Dependencies

	//using AdvancedInspector;
	using UnityEngine;
	using Zen.Common.ZenECS;

	#endregion

	public class WeaponComp : ComponentEcs
	{
		// Damage Information
		[ShowInInspector]
		public int ShieldDamage { get; set; }

		[ShowInInspector]
		public int HullDamage { get; set; }

		// Weapon Information
		[ShowInInspector]
		public float AttackRange { get; set; }

		[ShowInInspector]
		public float AttackRate { get; set; }

		[ShowInInspector]
		public WeaponTypes WeaponType { get; set; }

		[ShowInInspector]
		public WeaponResolutionTypes ResolutionType { get; set; }

		

		//[TextField(TextFieldType.Prefab, "Weapons/Launchers")] 
		public string LauncherPrefab = "None";

		[HideInInspector]public GameObject WeaponGameObject;

		// Projectile Information
		[ShowInInspector]
		public float ProjectileSpeed { get; set; }

		private bool ProjIsNotMissile()
		{
			return WeaponType != WeaponTypes.Missile;
		}

		private bool projectileIsEntity;

		[ShowIf("ProjIsNotMissile")]
		public bool ProjectileIsEntity
		{
			get { return (projectileIsEntity || WeaponType == WeaponTypes.Missile); }
			set { projectileIsEntity = value; }
		}

		private bool ProjectileUsesPrefab() { return !ProjectileIsEntity; }

		[ShowIf("ProjectileIsEntity")]
		//[TextField(TextFieldType.Entity)]
		public string ProjectileEntity { get; set; }

		[ShowIf("ProjectileUsesPrefab")] 
		//[TextField(TextFieldType.Prefab, "Weapons/Projectiles")] 
		public string ProjectilePrefab = "None";

		[HideInEditorMode, HideInPlayMode]
		public bool IsFitted { get; set; }

		[HideInEditorMode, HideInPlayMode]
		public float NextAttackTime { get; set; }

		[ShowInInspector]
		public ShipFitting fittingAttached { get; set; }

		public override ComponentTypes ComponentType => ComponentTypes.WeaponComp;
		public override string Grouping => "Weapons";
	}

	public enum DamageMode
	{
		Single,
		Area
	}

	public enum AttackTypes
	{
		RangedSplash,
		RangedPoint
	}

	public enum WeaponTypes
	{
		Laser,
		Missile,
		Flak,
		Beam
	}

	public enum WeaponResolutionTypes
	{
		Raycast,
		Projectile,
		Particle
	}

	public enum LaserFireType
	{
		None,
		FastLineRenderer,
		ProjectileGO,
		NormalLineRenderer,
		Particle
	}
}