﻿using Sirenix.OdinInspector;

namespace Zen.Components
{
	using System;
	using System.Collections.Generic;
	//using AdvancedInspector;
	using Common.ZenECS;
	using UnityEngine;

	public class ShipFittingsComp : ComponentEcs
	{
		[ShowInInspector]
		public List<ShipFitting> fittingList = new List<ShipFitting>();

		public override void Initialise(EcsEngine _engine, Entity owner)
		{
			base.Initialise(_engine, owner);
			foreach (var fitting in fittingList)
			{
				if (fitting.FittedWeapon != null)
				{
					fitting.FittedWeapon.fittingAttached = fitting;
				}
			}
		}

		public override ComponentTypes ComponentType => ComponentTypes.ShipFittingsComp;
		public override string Grouping => "Weapons";
	}

	[Serializable]
	public class ShipFitting
	{
		public ShipFitting()
		{
			PositionOffset = Vector3.zero;
			RotationOffset = Quaternion.identity;
		}

		[ShowInInspector] 
		//[CreateDerived]
		public WeaponComp FittedWeapon;
		[ShowInInspector] public bool IsEnabled;
		[ShowInInspector]public Vector3 PositionOffset;
		[ShowInInspector]public Quaternion RotationOffset;
		[ShowInInspector] [ReadOnly] public GameObject WeaponFittingGO;

		[ShowInInspector, ReadOnly]
		public Vector3 ProjectileSpawnPositionOffset;

		[ShowInInspector]public WeaponTypes WeaponTypesAllowed;
	}
}

//[CreateDerived]
//public List<WeaponComp> AvailableWeapons = new List<WeaponComp>();