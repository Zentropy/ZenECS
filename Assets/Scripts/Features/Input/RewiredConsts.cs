// <auto-generated>
// Rewired Constants
// This list was generated on 4/9/2018 8:37:18 PM
// The list applies to only the Rewired Input Manager from which it was generated.
// If you use a different Rewired Input Manager, you will have to generate a new list.
// If you make changes to the exported items in the Rewired Input Manager, you will
// need to regenerate this list.
// </auto-generated>

public static partial class RA {
    // Default
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Rotate Horizontal")]
    public const int RotateHorizontal = 0;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Rotate Horizontal")]
    public const int MouseRotateHorizontal = 16;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Rotate Vertically")]
    public const int RotateVertical = 1;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Rotate Vertically")]
    public const int MouseRotateVertical = 17;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Fire Primary")]
    public const int Fire1 = 2;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Fire Secondary")]
    public const int Fire2 = 3;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Roll")]
    public const int Roll = 4;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Zoom")]
    public const int Zoom = 5;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "MouseLook")]
    public const int MouseLook = 6;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Forward Thruster Acceleration")]
    public const int Accelerate = 7;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Open Cargo Interface")]
    public const int OpenCargo = 10;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Strafe Horizontally")]
    public const int StrafeHorizontal = 11;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Strafe Vertically")]
    public const int StrafeVertical = 12;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Toggle Inertial Dampering")]
    public const int InertialDampers = 13;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Full stop")]
    public const int FullHalt = 14;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "Click select a target")]
    public const int SelectTarget = 18;
    [Rewired.Dev.ActionIdFieldInfo(categoryName = "Default", friendlyName = "rotate ship immediately to target")]
    public const int DEBUG_LookAtTarget = 19;
}
public static partial class RAPlayer {
    [Rewired.Dev.PlayerIdFieldInfo(friendlyName = "System")]
    public const int System = 9999999;
    [Rewired.Dev.PlayerIdFieldInfo(friendlyName = "Player0")]
    public const int Player0 = 0;
}
