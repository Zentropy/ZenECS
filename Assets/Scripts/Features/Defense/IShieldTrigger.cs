﻿namespace Features.Defense
{
	public interface IShieldTrigger
	{
		void TriggerShield();
	}
}