﻿namespace Zen.Components
{
	using Common.ZenECS;

	public class InertialDamperModComp : AbstractModuleComp
	{
		public override ComponentTypes ComponentType => ComponentTypes.InertialDamperModComp;
	}
}