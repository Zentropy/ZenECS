﻿// /** 
//  * UnityPrefabComp.cs
//  * Dylan Bailey
//  * 20161209
// */

using Sirenix.OdinInspector;

namespace Zen.Components
{
	#region Dependencies

	//using AdvancedInspector;
	using UnityEngine;
	using Zen.Common.ZenECS;

	#endregion

	public class ShipPrefabComp : ComponentEcs
	{
		[ShowInInspector]
		//[TextField(TextFieldType.Prefab, "Ships")]
		public string ShipPrefab = "Prefabs/None";



		[ShowInInspector]
		public bool IsPooled { get; set; } = true;

		[ShowInInspector]
		public Vector3 FirstPersonCameraOffset { get; set; }

		public override ComponentTypes ComponentType => ComponentTypes.ShipPrefabComp;
		public override string Grouping => "Ship";
	}
}