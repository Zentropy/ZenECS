﻿namespace Zen.Components
{
	using Common.ZenECS;

	public class PlayerShipComp : ComponentEcs
	{
		public override ComponentTypes ComponentType => ComponentTypes.PlayerShipComp;
		public override string Grouping => "Player";
	}
}