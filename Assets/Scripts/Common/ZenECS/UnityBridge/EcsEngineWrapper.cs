﻿// /**
// * EcsEngineWrapper.cs
// * Dylan Bailey
// * 20161209
// */

using Sirenix.OdinInspector;

namespace Zen.Common.ZenECS
{
	#region Dependencies

	using Systems;
	//using AdvancedInspector;
	
	using UnityEngine;

	#endregion

    //[fiInspectorOnly]
	public class EcsEngineWrapper : MonoBehaviour
    {
        public string wrapperName = "Wrapper";
		[SerializeField]
        private EcsEngine _engine;

		[HideReferenceObjectPicker, HideLabel, HideInEditorMode]
        public EcsEngine engine
		{
			get { return _engine ?? (_engine = EcsEngine.Instance); }
		}

		protected void Awake()
		{
			EcsEngine.Instance.AddSystem(new GameInitSystem());

		}

		private void Update()
		{
			EcsEngine.Instance.Update();
		}

		private void FixedUpdate()
		{
			EcsEngine.Instance.FixedUpdate();
		}

		private void LateUpdate()
		{
			EcsEngine.Instance.LateUpdate();
		}
	}
}